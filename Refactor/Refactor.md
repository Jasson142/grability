import Foundation
import Async
class VideoMode {
    var savings: Float = 0.0
    var products: [MProduct] = []
    var exclusivePrice: Float = 0.0
    var serviceFee: Float = 0.0
}

class CartSync {
    func openDatabase() -> OpaquePointer {
        var db: OpaquePointer! = nil
        if sqlite3_open("db.sql", &db) == SQLITE_OK {
            print("Successfully opened connection to database at \(part1DbPath)")
            return db;
        } else {
            print("Unable to open database. Verify that you created the directory described " +
                "in the Getting Started section.")
        }
    }

    
    func ProductsByUUIDQuery(TheIds: Array, callback: (Void)){
        var queryStatement: OpaquePointer! = nil
        if sqlite3_prepare_v2(openDatabase(), queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            if sqlite3_step(queryStatement) == SQLITE_ROW {
                self.cart.products = dbProducts.map { product in
                    product.appOrigin = .Sync
                    for updateProduct in cart.products {
                        if updateProduct.upc == product.uuid {
                            if product.numOfProducts > 0 {
                                product.updateProductSync(sqlite3_column_int(queryStatement, 0))
                                product.totalGroup = sqlite3_column_int(queryStatement, 1) / 100
                                product.numOfProducts += 1
                                product.quantity = sqlite3_column_int(queryStatement, 2)
                            } else {
                                product.updateProductSync(updateProduct)
                                product.totalGroup = updateProduct.price / 100
                                product.numOfProducts += 1
                                product.quantity = updateProduct.quantity
                            }
                        }
                    }
                    return product
                }
                callback(.Ok)
                
            } else {
                callback(.FailValidation)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
    }
    var cart = VideoMode();
    var productsNotInDBids: Set<String> = []
    func updateWithCart(cart: APIResponse, callback: ((_ status: ServiceStatus) -> Void) ) {
        cart = cart;
        let ids = cart.getProductIds()
        ProductsByUUIDQuery(ids){ (result) -> () in
            // do stuff with the result
            Async.main {
                callback(status: result)
            }
        }
    }
    func getProductIds() -> [String] {
        return cart.products.flatMap { $0.identification }
    }
}