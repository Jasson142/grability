//
//  InitViewController.m
//  gability_test
//
//  Created by jasson on 6/21/17.
//  Copyright © 2017 gability. All rights reserved.
//

#import "InitViewController.h"

@interface InitViewController ()

@end

@implementation InitViewController
@synthesize ListMovies;
@synthesize delegate;
@synthesize letAddOnExists;
@synthesize totalPages;
@synthesize remain;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

-(void)delegateInitTop:(NSString *)category{
    
    self.totalPages = @"2";
    self.remain = 0;
    NSFileManager *manager = [NSFileManager defaultManager];
    BOOL isDirectory;
    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *yoyoDir = [documentsDirectoryPath stringByAppendingPathComponent:@"poster"];
    if (![manager fileExistsAtPath:yoyoDir isDirectory:&isDirectory] || !isDirectory) {
        NSError *error = nil;
        NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete
                                                         forKey:NSFileProtectionKey];
        [manager createDirectoryAtPath:yoyoDir
           withIntermediateDirectories:YES
                            attributes:attr
                                 error:&error];
        if (error)
            NSLog(@"Error creating directory path: %@", [error localizedDescription]);
    }
    letAddOnExists = true;
    self.ListMovies = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
    self.dbManager = [[Manager alloc] initWithDatabaseFilename:@"db.sql"];
    NSString *query = [NSString stringWithFormat: @"select * from movies WHERE category = '%@' ORDER BY popularity ASC", category];
    NSArray *arrPeopleInfo = nil;
    arrPeopleInfo = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    if(arrPeopleInfo || arrPeopleInfo.count){
        self.ListMovies = [[NSMutableArray alloc] initWithArray:arrPeopleInfo copyItems:YES];
        letAddOnExists = false;
        [delegate reloadData:self.ListMovies];
    }
    
    dispatch_queue_t queue = dispatch_queue_create("test", NULL);
    dispatch_async(queue, ^{
        for (int i = 1; i < [self.totalPages intValue]; i++) {
            NSError *error;
            NSString *url_string = [NSString stringWithFormat: @"https://api.themoviedb.org/3/movie/%@?api_key=ff17be5f33ecef90e365ed15bab2eacc&language=en-US&page=%d",category, i];
            NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
            if(data){
                NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                //NSLog(@"json: %@", json);
                if(![self.totalPages isEqualToString:[NSString stringWithFormat:@"%@", [json valueForKey:@"total_pages"]]]){
                    self.totalPages = [NSString stringWithFormat:@"%@", [json valueForKey:@"total_pages"]];
                }
                for(NSDictionary *item in [json valueForKey:@"results"]) {
                    NSString *query = [NSString stringWithFormat: @"select * from movies WHERE id = %d" , [item[@"id"] intValue]];
                    NSArray *arrPeopleInfo = nil;
                    arrPeopleInfo = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
                    
                    if(!arrPeopleInfo || !arrPeopleInfo.count){
                        query = [NSString stringWithFormat:@"insert into movies(id, title, popularity, poster_path, overview, release_date, category) values(%d, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", [item[@"id"] intValue], item[@"title"], item[@"popularity"], item[@"poster_path"], [item[@"overview"] stringByReplacingOccurrencesOfString:@"\"" withString:@"'"], item[@"release_date"], category];
                        // Execute the query.
                        [self.dbManager executeQuery:query];
                        
                        // If the query was successfully executed then pop the view controller.
                        if (self.dbManager.affectedRows != 0) {
                            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                            
                            // Pop the view controller.
                            //[self.navigationController popViewControllerAnimated:YES];
                            //self.delegate
                            //self.ListMovies addObjectsFromArray:<#(nonnull NSArray *)#>
                            //[self.ListMovies addObject:arrPeopleInfo];
                            arrPeopleInfo = [NSArray arrayWithObjects:item[@"id"], item[@"title"],item[@"popularity"], item[@"poster_path"], [item[@"overview"] stringByReplacingOccurrencesOfString:@"\"" withString:@"'"], item[@"release_date"], nil];
                            [self saveImagesInLocalDirectory:item[@"poster_path"] Id:item[@"id"]];
                            [self.ListMovies addObject:arrPeopleInfo];
                            //[delegate insertData:[[NSMutableArray alloc] initWithArray:arrPeopleInfo copyItems:YES]];
                            remain = remain + 1;
                            [delegate tellText:[NSString stringWithFormat:@"%d", remain]];
                        }
                        else{
                            NSLog(@"Could not execute the query.");
                            NSLog(@"query: %@", query);
                        }
                        
                    }else if(self.letAddOnExists){
                        NSLog(@"EXISTS");
                        if([[arrPeopleInfo objectAtIndex:6] isEqualToString:category]){
                            query = [NSString stringWithFormat:@"Update movies set category = '%@' Where id = %d",category, [[arrPeopleInfo objectAtIndex:0] intValue]];
                            // Execute the query.
                            [self.dbManager executeQuery:query];
                            
                            // If the query was successfully executed then pop the view controller.
                            if (self.dbManager.affectedRows != 0) {
                                NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                            }
                        }
                        [self saveImagesInLocalDirectory:[arrPeopleInfo objectAtIndex:3] Id:[arrPeopleInfo objectAtIndex:0]];
                        [self.ListMovies addObjectsFromArray:arrPeopleInfo];
                        NSLog(@"object : %@", self.ListMovies);
                        remain = remain + 1;
                        [delegate tellText:[NSString stringWithFormat:@"%d", remain]];
                    }
                    
                }
            }
        }
        
        
        //code to be executed in the background
        dispatch_async(dispatch_get_main_queue(), ^{
            //code to be executed on the main thread when background task is finished
            NSLog(@"finish: ");
            [delegate reloadData:self.ListMovies];
            [delegate tellUnAnim];
        
        });
    });
}

-(void)delegateInitUp:(NSString *)category{
    
    self.totalPages = @"2";
    self.remain = 0;
    NSFileManager *manager = [NSFileManager defaultManager];
    BOOL isDirectory;
    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *yoyoDir = [documentsDirectoryPath stringByAppendingPathComponent:@"poster"];
    if (![manager fileExistsAtPath:yoyoDir isDirectory:&isDirectory] || !isDirectory) {
        NSError *error = nil;
        NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete
                                                         forKey:NSFileProtectionKey];
        [manager createDirectoryAtPath:yoyoDir
           withIntermediateDirectories:YES
                            attributes:attr
                                 error:&error];
        if (error)
            NSLog(@"Error creating directory path: %@", [error localizedDescription]);
    }
    letAddOnExists = true;
    self.ListMovies = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
    self.dbManager = [[Manager alloc] initWithDatabaseFilename:@"db.sql"];
    NSString *query = [NSString stringWithFormat: @"select * from movies WHERE category = '%@' ORDER BY popularity ASC", category];
    NSArray *arrPeopleInfo = nil;
    arrPeopleInfo = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    if(arrPeopleInfo || arrPeopleInfo.count){
        self.ListMovies = [[NSMutableArray alloc] initWithArray:arrPeopleInfo copyItems:YES];
        letAddOnExists = false;
        [delegate reloadData:self.ListMovies];
    }
    
    dispatch_queue_t queue = dispatch_queue_create("test", NULL);
    dispatch_async(queue, ^{
        for (int i = 1; i < [self.totalPages intValue]; i++) {
            NSError *error;
            NSString *url_string = [NSString stringWithFormat: @"https://api.themoviedb.org/3/movie/%@?api_key=ff17be5f33ecef90e365ed15bab2eacc&language=en-US&page=%d",category, i];
            NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
            if(data){
                NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                //NSLog(@"json: %@", json);
                if(![self.totalPages isEqualToString:[NSString stringWithFormat:@"%@", [json valueForKey:@"total_pages"]]]){
                    self.totalPages = [NSString stringWithFormat:@"%@", [json valueForKey:@"total_pages"]];
                }
                for(NSDictionary *item in [json valueForKey:@"results"]) {
                    NSString *query = [NSString stringWithFormat: @"select * from movies WHERE id = %d" , [item[@"id"] intValue]];
                    NSArray *arrPeopleInfo = nil;
                    arrPeopleInfo = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
                    
                    if(!arrPeopleInfo || !arrPeopleInfo.count){
                        query = [NSString stringWithFormat:@"insert into movies(id, title, popularity, poster_path, overview, release_date, category) values(%d, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", [item[@"id"] intValue], item[@"title"], item[@"popularity"], item[@"poster_path"], [item[@"overview"] stringByReplacingOccurrencesOfString:@"\"" withString:@"'"], item[@"release_date"], category];
                        // Execute the query.
                        [self.dbManager executeQuery:query];
                        
                        // If the query was successfully executed then pop the view controller.
                        if (self.dbManager.affectedRows != 0) {
                            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                            
                            // Pop the view controller.
                            //[self.navigationController popViewControllerAnimated:YES];
                            //self.delegate
                            //self.ListMovies addObjectsFromArray:<#(nonnull NSArray *)#>
                            //[self.ListMovies addObject:arrPeopleInfo];
                            arrPeopleInfo = [NSArray arrayWithObjects:item[@"id"], item[@"title"],item[@"popularity"], item[@"poster_path"], [item[@"overview"] stringByReplacingOccurrencesOfString:@"\"" withString:@"'"], item[@"release_date"], nil];
                            [self saveImagesInLocalDirectory:item[@"poster_path"] Id:item[@"id"]];
                            [self.ListMovies addObject:arrPeopleInfo];
                            //[delegate insertData:[[NSMutableArray alloc] initWithArray:arrPeopleInfo copyItems:YES]];
                            remain = remain + 1;
                            [delegate tellText:[NSString stringWithFormat:@"%d", remain]];
                        }
                        else{
                            NSLog(@"Could not execute the query.");
                            NSLog(@"query: %@", query);
                        }
                        
                    }else if(self.letAddOnExists){
                        NSLog(@"EXISTS");
                        if([[arrPeopleInfo objectAtIndex:6] isEqualToString:category]){
                            query = [NSString stringWithFormat:@"Update movies set category = '%@' Where id = %d",category, [[arrPeopleInfo objectAtIndex:0] intValue]];
                            // Execute the query.
                            [self.dbManager executeQuery:query];
                            
                            // If the query was successfully executed then pop the view controller.
                            if (self.dbManager.affectedRows != 0) {
                                NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                            }
                        }
                        [self saveImagesInLocalDirectory:[arrPeopleInfo objectAtIndex:3] Id:[arrPeopleInfo objectAtIndex:0]];
                        [self.ListMovies addObjectsFromArray:arrPeopleInfo];
                        NSLog(@"object : %@", self.ListMovies);
                        remain = remain + 1;
                        [delegate tellText:[NSString stringWithFormat:@"%d", remain]];
                    }
                    
                }
            }
        }
        
        
        //code to be executed in the background
        dispatch_async(dispatch_get_main_queue(), ^{
            //code to be executed on the main thread when background task is finished
            NSLog(@"finish: ");
            [delegate reloadData:self.ListMovies];
            [delegate tellUnAnim];
        });
    });
}

-(void)delegateInit:(NSString *)category{
    
    self.totalPages = @"2";
    self.remain = 0;
    NSFileManager *manager = [NSFileManager defaultManager];
    BOOL isDirectory;
    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *yoyoDir = [documentsDirectoryPath stringByAppendingPathComponent:@"poster"];
    if (![manager fileExistsAtPath:yoyoDir isDirectory:&isDirectory] || !isDirectory) {
        NSError *error = nil;
        NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete
                                                         forKey:NSFileProtectionKey];
        [manager createDirectoryAtPath:yoyoDir
           withIntermediateDirectories:YES
                            attributes:attr
                                 error:&error];
        if (error)
            NSLog(@"Error creating directory path: %@", [error localizedDescription]);
    }
    letAddOnExists = true;
    self.ListMovies = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
    self.dbManager = [[Manager alloc] initWithDatabaseFilename:@"db.sql"];
    NSString *query = [NSString stringWithFormat: @"select * from movies WHERE category = '%@' ORDER BY popularity ASC", category];
    NSArray *arrPeopleInfo = nil;
    arrPeopleInfo = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    if(arrPeopleInfo || arrPeopleInfo.count){
        self.ListMovies = [[NSMutableArray alloc] initWithArray:arrPeopleInfo copyItems:YES];
        letAddOnExists = false;
        [delegate reloadData:self.ListMovies];
    }

    dispatch_queue_t queue = dispatch_queue_create("test", NULL);
    dispatch_async(queue, ^{
        for (int i = 1; i < [self.totalPages intValue]; i++) {
            NSError *error;
            NSString *url_string = [NSString stringWithFormat: @"https://api.themoviedb.org/3/movie/%@?api_key=ff17be5f33ecef90e365ed15bab2eacc&language=en-US&page=%d",category, i];
            NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
            if(data){
                NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                //NSLog(@"json: %@", json);
                if(![self.totalPages isEqualToString:[NSString stringWithFormat:@"%@", [json valueForKey:@"total_pages"]]]){
                    self.totalPages = [NSString stringWithFormat:@"%@", [json valueForKey:@"total_pages"]];
                }
                for(NSDictionary *item in [json valueForKey:@"results"]) {
                    NSString *query = [NSString stringWithFormat: @"select * from movies WHERE id = %d" , [item[@"id"] intValue]];
                    NSArray *arrPeopleInfo = nil;
                    arrPeopleInfo = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
                    
                    if(!arrPeopleInfo || !arrPeopleInfo.count){
                        query = [NSString stringWithFormat:@"insert into movies(id, title, popularity, poster_path, overview, release_date, category) values(%d, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", [item[@"id"] intValue], item[@"title"], item[@"popularity"], item[@"poster_path"], [item[@"overview"] stringByReplacingOccurrencesOfString:@"\"" withString:@"'"], item[@"release_date"], category];
                        // Execute the query.
                        [self.dbManager executeQuery:query];
                        
                        // If the query was successfully executed then pop the view controller.
                        if (self.dbManager.affectedRows != 0) {
                            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                            
                            // Pop the view controller.
                            //[self.navigationController popViewControllerAnimated:YES];
                            //self.delegate
                            //self.ListMovies addObjectsFromArray:<#(nonnull NSArray *)#>
                            //[self.ListMovies addObject:arrPeopleInfo];
                            arrPeopleInfo = [NSArray arrayWithObjects:item[@"id"], item[@"title"],item[@"popularity"], item[@"poster_path"], [item[@"overview"] stringByReplacingOccurrencesOfString:@"\"" withString:@"'"], item[@"release_date"], nil];
                            [self saveImagesInLocalDirectory:item[@"poster_path"] Id:item[@"id"]];
                            [self.ListMovies addObject:arrPeopleInfo];
                            //[delegate insertData:[[NSMutableArray alloc] initWithArray:arrPeopleInfo copyItems:YES]];
                            remain = remain + 1;
                            [delegate tellText:[NSString stringWithFormat:@"%d", remain]];
                        }
                        else{
                            NSLog(@"Could not execute the query.");
                            NSLog(@"query: %@", query);
                        }
                        
                    }else if(self.letAddOnExists){
                        NSLog(@"EXISTS");
                        if([[arrPeopleInfo objectAtIndex:6] isEqualToString:category]){
                            query = [NSString stringWithFormat:@"Update movies set category = '%@' Where id = %d",category, [[arrPeopleInfo objectAtIndex:0] intValue]];
                            // Execute the query.
                            [self.dbManager executeQuery:query];
                            
                            // If the query was successfully executed then pop the view controller.
                            if (self.dbManager.affectedRows != 0) {
                                NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                            }
                        }
                        [self saveImagesInLocalDirectory:[arrPeopleInfo objectAtIndex:3] Id:[arrPeopleInfo objectAtIndex:0]];
                        [self.ListMovies addObjectsFromArray:arrPeopleInfo];
                        NSLog(@"object : %@", self.ListMovies);
                        remain = remain + 1;
                        [delegate tellText:[NSString stringWithFormat:@"%d", remain]];
                    }
                    
                }
            }
        }
        
        
        //code to be executed in the background
        dispatch_async(dispatch_get_main_queue(), ^{
            //code to be executed on the main thread when background task is finished
            NSLog(@"finish: ");
            [delegate reloadData:self.ListMovies];
            [delegate tellUnAnim];
        });
    });
}

-(void)forceReload{
    remain = 0;
    [delegate tellText:[NSString stringWithFormat:@"%d", remain]];
    [delegate reloadData:self.ListMovies];
}

-(void)searchData:(NSString *) What Category:(NSString *)category{
    //instanciate for avoid alloc memory issues
    Manager *SearchManager = [[Manager alloc] initWithDatabaseFilename:@"db.sql"];
    NSString *query = [NSString stringWithFormat: @"select * from movies WHERE title LIKE '%%%@%%' AND category = '%@' ORDER BY popularity ASC", What, category];
    NSArray *arrPeopleInfo = nil;
    arrPeopleInfo = [[NSArray alloc] initWithArray:[SearchManager loadDataFromDB:query]];
    if(arrPeopleInfo || arrPeopleInfo.count){
        self.totalPages = @"0";
        [self.ListMovies removeAllObjects];
        self.ListMovies = [[NSMutableArray alloc] initWithArray:arrPeopleInfo copyItems:YES];
        [delegate reloadData:self.ListMovies];
    }
}

-(void)initData:(NSString *)category{
    //instanciate for avoid alloc memory issues
    Manager *SearchManager = [[Manager alloc] initWithDatabaseFilename:@"db.sql"];
    NSString *query = [NSString stringWithFormat: @"select * from movies WHERE category = '%@' ORDER BY popularity ASC", category];
    NSArray *arrPeopleInfo = nil;
    arrPeopleInfo = [[NSArray alloc] initWithArray:[SearchManager loadDataFromDB:query]];
    if(arrPeopleInfo || arrPeopleInfo.count){
        self.totalPages = @"0";
        [self.ListMovies removeAllObjects];
        self.ListMovies = [[NSMutableArray alloc] initWithArray:arrPeopleInfo copyItems:YES];
        [delegate reloadData:self.ListMovies];
    }
}

-(void)saveImagesInLocalDirectory:(NSString *)Url Id:(NSString *)Id
{
    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *imgName = [NSString stringWithFormat:@"poster/%@.png", Id];
    NSString *imgURL = [NSString stringWithFormat:@"http://image.tmdb.org/t/p/w185/%@", Url];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *writablePath = [documentsDirectoryPath stringByAppendingPathComponent:imgName];
    
    if(![fileManager fileExistsAtPath:writablePath]){
        // file doesn't exist
        NSLog(@"file doesn't exist");
        //save Image From URL
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL]];
        
        NSError *error = nil;
        [data writeToFile:[documentsDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName]] options:NSAtomicWrite error:&error];
        
        if (error) {
            NSLog(@"Error Writing File : %@",error);
        }else{
            NSLog(@"Image %@ Saved SuccessFully",imgName);
        }
    }
    else{
        // file exist
        NSLog(@"file exist");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
