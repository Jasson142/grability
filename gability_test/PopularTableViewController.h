//
//  PopularTableViewController.h
//  gability_test
//
//  Created by jasson on 6/21/17.
//  Copyright © 2017 gability. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InitViewController.h"
#import "CustomViewCell.h"
#import "ViewController.h"
#import "TabBarViewController.h"



@interface PopularTableViewController : UITableViewController<InitViewControllerDelegate, UISearchBarDelegate>
@property (nonatomic, strong) NSMutableArray *DataTable;
@property (nonatomic, weak) IBOutlet UISearchBar *Search;
@property (nonatomic, strong) NSString *searchText;
@end
