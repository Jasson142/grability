//
//  ViewController.h
//  gability_test
//
//  Created by jasson on 6/21/17.
//  Copyright © 2017 gability. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic, weak) IBOutlet UILabel *Tittle;
@property (nonatomic, weak) IBOutlet UILabel *Popularity;
@property (nonatomic, weak) IBOutlet UITextView *OverView;
@property (nonatomic, weak) IBOutlet UIImageView *Img;
@property (nonatomic, strong) NSString *ImgPath;
@property (strong, nonatomic) NSMutableArray *Data;

@end

