//
//  InitViewController.h
//  gability_test
//
//  Created by jasson on 6/21/17.
//  Copyright © 2017 gability. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Manager.h"


@protocol InitViewControllerDelegate<NSObject>
-(void)reloadData:(NSMutableArray *) Data;
-(void)insertData:(NSMutableArray *) Data;
-(void)tellUnAnim;
-(void)tellNumberItems;
-(void)tellText:(NSString *)text;
@end

@interface InitViewController : UINavigationController
@property (nonatomic, strong) Manager *dbManager;
@property (nonatomic, assign) Boolean letAddOnExists;
@property (nonatomic, strong) NSMutableArray *ListMovies;
@property (nonatomic, strong) NSString *totalPages;
@property (nonatomic, assign) int remain;
@property (nonatomic, weak) id<InitViewControllerDelegate> delegate;
-(void)delegateInit:(NSString *)category;
-(void)delegateInitTop:(NSString *)category;
-(void)delegateInitUp:(NSString *)category;
-(void)searchData:(NSString *) What;
-(void)initData:(NSString *)category;
-(void)forceReload;
@end
