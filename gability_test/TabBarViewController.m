//
//  TabBarViewController.m
//  gability_test
//
//  Created by jasson on 6/21/17.
//  Copyright © 2017 gability. All rights reserved.
//

#import "TabBarViewController.h"


@interface TabBarViewController ()

@end

@implementation TabBarViewController
@synthesize indicator;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIView *content = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40)];
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator setFrame:CGRectMake(0, 0, 40, 40)];
    UILabel *number = [[UILabel alloc] initWithFrame:CGRectMake((self.view.bounds.size.width / 2) - 60, 0, 100, 40)];
    number.text = @"0 Movies for reload";
    [number setAdjustsFontSizeToFitWidth:YES];
    number.tag = 1;
    [content addSubview:number];
    [content addSubview:indicator];
    self.navigationItem.titleView = content;
    
//
//    UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonItemStylePlain target:self action:@selector(refresh:)];
//    //[button setFrame:CGRectMake(0, 0, 40, 40)];
//    [button addSubview:number];
//    self.navigationItem.leftBarButtonItem = button;
    
}

-(void)setText:(NSString *) text{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        UILabel *number = [self.navigationItem.titleView viewWithTag:1];
        number.text = [NSString stringWithFormat:@"%@ Movies for reload", text];
        [self.navigationItem.titleView setNeedsDisplay];
    });
}

-(void)animate{
    [indicator startAnimating];
}

-(void)unanimate{
    [indicator stopAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    
    //[indicator startAnimating];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
