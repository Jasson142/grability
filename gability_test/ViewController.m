    //
//  ViewController.m
//  gability_test
//
//  Created by jasson on 6/21/17.
//  Copyright © 2017 gability. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize Data;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"data: %@", self.Data);
    self.Tittle.text = [self.Data objectAtIndex:1];
    //http://image.tmdb.org/t/p/w185/
    self.Popularity.text = [NSString stringWithFormat:@"%@", [self.Data objectAtIndex:2]];
    self.OverView.text = [self.Data objectAtIndex:4];
    self.ImgPath = [self.Data objectAtIndex:3];
    
    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *imgName = [NSString stringWithFormat:@"poster/%@.png", [self.Data objectAtIndex:0]];
    NSString *imgURL = [NSString stringWithFormat:@"https://image.tmdb.org/t/p/w185/%@", self.ImgPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *writablePath = [documentsDirectoryPath stringByAppendingPathComponent:imgName];
    
    [self.Img setImage: [UIImage imageNamed:@"default"]];
    if(![fileManager fileExistsAtPath:writablePath]){
        // file doesn't exist
        NSLog(@"file doesn't exist");
        //save Image From URL
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL]];
        if(data){
            NSError *error = nil;
            [data writeToFile:[documentsDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName]] options:NSAtomicWrite error:&error];
            
            if (error) {
                NSLog(@"Error Writing File : %@",error);
                
            }else{
                NSLog(@"Image %@ Saved SuccessFully",imgName);
                [self.Img setImage: [UIImage imageWithContentsOfFile:[documentsDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName]]]];
            }
        }
        
    }
    else{
        // file exist
        NSLog(@"file exist");
        [self.Img setImage: [UIImage imageWithContentsOfFile:[documentsDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName]]]];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
