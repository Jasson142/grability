//
//  UpTableViewController.m
//  gability_test
//
//  Created by jasson on 6/23/17.
//  Copyright © 2017 gability. All rights reserved.
//

#import "UpTableViewController.h"

@interface UpTableViewController ()

@end

@implementation UpTableViewController

@synthesize DataTable;
@synthesize Search;
@synthesize searchText;
- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh:)];
    TabBarViewController *tab = (TabBarViewController *)self.navigationController.viewControllers.firstObject;
    [tab animate];
    tab.navigationItem.rightBarButtonItem = button;
    
    [(InitViewController *)self.navigationController setDelegate:self];
    [(InitViewController *)self.navigationController delegateInitUp:@"upcoming"];
    
    
}

- (void) refresh:(id)sender{
    [(InitViewController *)self.navigationController forceReload];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

-(void)reloadData:(NSMutableArray *)Data{
    self.searchText = @"";
    self.DataTable = Data;
    //NSLog(@"ENDDDD : %@", self.DataTable);
    [self.tableView reloadData];
}

-(void)tellText:(NSString *)text{
    [(TabBarViewController *)self.navigationController.viewControllers.firstObject setText:text];
}

-(void)tellUnAnim{
    [(TabBarViewController *)self.navigationController.viewControllers.firstObject unanimate];
}

-(void)tellNumberItems{
    [(TabBarViewController *)self.navigationController.viewControllers.firstObject unanimate];
}

-(void)insertData:(NSMutableArray *)Data{
    [self.tableView beginUpdates];
    [self.DataTable insertObject:Data atIndex:self.DataTable.count];
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:[self.DataTable count]-1 inSection:0]];
    [[self tableView] insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationTop];
    [self.tableView endUpdates];
    [self.tableView setContentSize:CGSizeMake(self.tableView.contentSize.width, self.tableView.contentSize.height + 60.0)];
}

- (void)viewWillAppear:(BOOL)animated{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.DataTable.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *ID = @"CustomViewCell";
    CustomViewCell *cell = (CustomViewCell *)[tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:ID owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    //NSLog(@"expr : %@", [[[self.DataTable objectAtIndex:indexPath.row] objectAtIndex:0] objectAtIndex:1]);
    
    cell.Tittle.text = [[self.DataTable objectAtIndex:indexPath.row] objectAtIndex:1];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    // Configure the cell...
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.userInteractionEnabled = YES;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld", (long)indexPath.row); // you can see selected row number in your console;
    [self performSegueWithIdentifier:@"MovieDetail" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ViewController *detailVC = segue.destinationViewController;
    NSIndexPath *selectedPath = [self.tableView indexPathForSelectedRow];
    detailVC.Data = [self.DataTable objectAtIndex:selectedPath.row];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

#pragma mark - SearchBar Delegate Methods

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.searchText = searchText;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)SearchBar
{
    SearchBar.showsCancelButton=YES;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)theSearchBar
{
    [theSearchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)SearchBar
{
    @try
    {
        SearchBar.showsCancelButton=NO;
        [SearchBar resignFirstResponder];
        [(InitViewController *)self.navigationController initData:@"top_rated"];
        [self.tableView reloadData];
    }
    @catch (NSException *exception) {
    }
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)SearchBar
{
    [SearchBar resignFirstResponder];
    @try
    {
        //[self.DataTable removeAllObjects];
        [(InitViewController *)self.navigationController searchData:self.searchText];
    }
    @catch (NSException *exception) {
    }
}
@end
