//
//  TabBarViewController.h
//  gability_test
//
//  Created by jasson on 6/21/17.
//  Copyright © 2017 gability. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol TabBarViewControllerDelegate<NSObject>

@end

@interface TabBarViewController : UITabBarController
@property (strong, nonatomic) UIActivityIndicatorView *indicator;
@property (nonatomic, assign) id<TabBarViewControllerDelegate> delegate;
-(void) animate;
-(void) unanimate;
-(void)setText:(NSString *) text;
@end
