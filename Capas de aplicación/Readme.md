gability_test App

*	Requerimientos:
		IOS > 8.0
*	Lenguaje
	Objective - C
*	Estructura

	Table - Almacena las clases necesarias para las tablas
		Cell - Almacena las clases y xib para una celda personalizada
			CustomViewCell(.h .m) - Clase encargada de manejar el UI de la celda de la tabla
			CustomViewCell.xib - UI de la celda presonalizada conectada con la clase CustomViewCell
		PopularTableViewController(.h .m) - Clase encargada del manejo de datos de la tabla, ademas de eso es un InitViewControllerDelegate. en donde se muetran los datos de las peliculas populares
	Data - Almacena las clases y archivos necesarios para el manejo de las bases de datos
		db.sql - Base de datos sqlite con la siguiente estructura: CREATE TABLE movies(id INTEGER PRIMARY KEY AUTOINCREMENT, title VARCHAR(255), popularity VARCHAR(255), poster_path VARCHAR(255), overview TEXT, release_date VARCHAR(255), , category VARCHAR(255));
		Manager(.h .m) - Clase encargada del manejo de la base de datos tanto la inicializacion como la ejecucion de querys.
	Utils - Almacena los datos que xcode agrega por defecto
		Assets - Almacena las imagenes que se precargaran a la hora de instalar la app, se agrego una imagen default en caso de que no se descarge la imagen del poster de la pelicula
		Supporting Files - almacena la clase main de la aplicacion
	Views - Almacena todo el UI de la aplicacion
	Products - .app por defecto de xcode
	Frameworks - se utilizo sqlite3 para el manejo de la base de datos que fue solicitada
	InitViewController - este es el core de la aplicacion es al vista inicial y la vista core; loq ue quiero decir es que mediante la funcion -(void)delegateInit:(NSString *)category el va a cargar inicialmente los datos en base de datos y asincronicamente el va cargando los datos que va recogiendo de el api, tambien al ser consultado el api el guarda el poster de la imagen para poder verse en offline con esto el actualiza el header para que muestre al usaurio final el numero de registros por recargar y un loader indicando que se estan cargando datos, una vez presionado el boton de actualizar la tabla actualiza los datos de la base de datos haciendolos visibles en la tabla. Todo esto mediante delegados indicados a la clase PopulaTableViewcontroller.
	AppDelegate (.h .m) - nada nuevo
	ViewController(.h .m) - esta clase se encarga de manejar los datos enviados desde el segue de PopulartableViewcontroller  para mostrarlos en un UI donde se podra ver el titulo, la popularidad, un overview, y el poster de la pelicula el cual al no encontrase on al no poderse descargar mostrara una imagen por defecto que indica que no se ha encontrado la imagen correspondiente.
	TabBarViewcontroller(.h .m) - Esta clase se encarga de manejar el tabView de la aplicacion lo que hacemos aqui es manejar la carga dinamica en el UI de los datos que se estan cargando desde el api como es el indicador, el texto que indica cuantos registras faltan por cargar y el manejo del boton de recargar

* Comentarios
	Por falta de tiempo no logre terminar de integrar las tablas upcoming y top rated por cuestion de estabilidad entonces preferi dejar solo una estable